#!/bin/sh



# Define variables
NAME="yigit_unlu"
DAY=$(date +%d)
MONTH=$(date +%b)


if [ -z "$BUILD_NUMBER" ]; then
  echo "BUILD_NUMBER not set. Defaulting to 0."
  BUILD_NUMBER="0"
else
  echo "BUILD_NUMBER is set to $BUILD_NUMBER"
fi



# Construct the output job name
JOB_NAME="${NAME}_${BUILD_NUMBER}_${MONTH}${DAY}"


# Default value for output directory
OUTPUT_DIR="out"

# Parse arguments
while [ $# -gt 0 ]; do
  case "$1" in
    --output-directory=*)
      OUTPUT_DIR="${1#*=}" # Extract value after '='
      ;;
    *)
      echo "Unknown argument: $1"
      exit 1
      ;;
  esac
  shift
done

echo "Runjob: Using output directory: $OUTPUT_DIR"


# Run lualatex with the constructed job name
lualatex --jobname="$JOB_NAME" --output-directory="$OUTPUT_DIR" main.tex



